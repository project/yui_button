YUI:
-----
http://developer.yahoo.com/yui
http://developer.yahoo.com/yui/button/


Installation:
-----
There is a link to a video that shows how to install the module
on the project homepage: http://www.drupal.org/project/yui_button

Author:
-----
Jeff Decker <jeff at jeffcd dot com>

Sponsor:
-----
<none />
